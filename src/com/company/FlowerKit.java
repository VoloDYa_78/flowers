package com.company;

public  class FlowerKit implements Flowers{

    private String type;
    private String color;

    private int cost;
    private int count;


    public String getType() {
        return type;
    }

    public String getColor() {
        return color;
    }

    public int getCost() {
        return cost;
    }

    public int getCount() {
        return count;
    }

    public FlowerKit(String type, String color, int cost, int count) {
        this.type = type;
        this.color = color;
        this.cost = cost;
        this.count = count;
    }

    @Override
    public int kitPrice() {
        return cost * count;
    }
}
