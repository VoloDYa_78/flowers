package com.company;

public class KitBuilder {
    private KitBuilder() {
    }

    private static KitBuilder instance;

    public static KitBuilder getInstance() {
        if (instance == null) {
            instance = new KitBuilder();
        }

        return instance;
    }

    public FlowerKit buildKit(String type, String color, int cost, int count) {
        var kit = new FlowerKit(type, color, cost, count);

        return kit;
    }


}
