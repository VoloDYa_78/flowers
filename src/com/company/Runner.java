package com.company;

public class Runner {

    private Runner() { }

    private static Runner instance;

    public static Runner getInstance() {
        if (instance == null) {
            instance = new Runner();
        }

        return instance;
    }

    public void start(){
        Florist florist = new Florist();
        florist.buildBouquet();
        florist.showBouquet();
        System.out.println();

        System.out.println("Let's sort it by price");
        florist.sortByPrice(Bouquet.getInstance().flowers);
        florist.showBouquet();
        System.out.println();

        System.out.println("Let's sort it by count");
        florist.sortByCount(Bouquet.getInstance().flowers);
        florist.showBouquet();
        System.out.println();
    }
}
