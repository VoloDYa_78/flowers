package com.company;

import java.util.ArrayList;

public class Bouquet {

    private Bouquet() {
    }

    private static Bouquet instance;

    public static Bouquet getInstance() {
        if (instance == null) {
            instance = new Bouquet();
        }

        return instance;
    }

    public ArrayList<FlowerKit> flowers = new ArrayList<>();
    ;


}
