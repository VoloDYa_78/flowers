package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Florist {
    public void buildBouquet() {
        Bouquet.getInstance().flowers.add(KitBuilder.getInstance().buildKit("roses", "white", 5, 10));
        Bouquet.getInstance().flowers.add(KitBuilder.getInstance().buildKit("begonia", "red", 2, 5));
        Bouquet.getInstance().flowers.add(KitBuilder.getInstance().buildKit("tulips", "white", 1, 7));
        Bouquet.getInstance().flowers.add(KitBuilder.getInstance().buildKit("hydrangea", "pink", 3, 8));
    }

    public void sortByPrice(ArrayList<FlowerKit> flowers) {
        Collections.sort(flowers, Comparator.comparingInt(flower -> flower.getCost()));
    }

    public void sortByCount(ArrayList<FlowerKit> flowers) {
        Collections.sort(flowers, Comparator.comparingInt(flower -> flower.getCount()));
    }

    public void showBouquet() {

        String message = new String();

        System.out.println("In your bouquet: ");

        for (FlowerKit flowerKit : Bouquet.getInstance().flowers) {
            message = flowerKit.getCount() + " "
                    + flowerKit.getColor() + " "
                    + flowerKit.getType() + "\n"
                    + "price of kit:" + flowerKit.kitPrice();

            System.out.println(message);
            System.out.println();
        }
    }
}
